extends KinematicBody2D

var Bullet = preload("res://game/scene/game/action/Laser.tscn")
var class_target = preload("res://addons/curve/curve_node.tscn")
var node_hud = preload("res://game/scene/ui/hud/Player_0.tscn")
onready var walk = $walk/walk

var speed = 200  # How fast the player will move (pixels/sec).
var directions = ["right", "bottom-right", "bottom", "bottom-left", "left", "top-left", "top", "top-right"]
var facing = Vector2(0, 0)
var direction = Vector2(0,1)
var contact # body to player entered
var occuped = false

var player #player data end control
var node_target
var selected_target
var main = false #pour savoir si le player est celui qu'on dirige

var target = Vector2()
var points = []
const eps = 1.3

func init(ui, data):
    player = load("res://game/util/resource/player_class.gd").new(ui, data)
  
func _ready():
    $Area2D3/Sprite/AnimationPlayer.play("rotation") #animation du cercle de detection
    if player && player.main == true:
        $Camera2D.current = true
        
func _input(event):
    if event.is_action_pressed("ui_fire"):
            fire(direction)
        
func _process(delta):
    OS.set_window_title("Prototype | fps: " + str(Engine.get_frames_per_second()))
    var move
    var velocity = Vector2(0, 0)
    var move_direction = Vector2(0, 0)
    var left = Input.is_action_pressed("ui_left")
    var right = Input.is_action_pressed("ui_right")
    var up = Input.is_action_pressed("ui_up")
    var down = Input.is_action_pressed("ui_down")
    
    if right:
        velocity.x += 1
    if left:
        velocity.x -= 1
    if down:
        velocity.y += 1
    if up:
        velocity.y -= 1   
    if Input.is_action_pressed('click'):
        target = get_global_mouse_position() 
    if velocity.length() > 0:
        velocity = velocity.normalized()
                    
    move_direction.x = int(right) - int(left)
    move_direction.y = int(down) - int(up)
    
    if left || right || up || down:
        target = null
        direction = move_direction
        var animation = direction2str(direction)
        walk.play("default-" + animation + "-8")
        $Area2D.rotation = move_direction.angle()
        move = move_and_slide(velocity * speed)
    elif target == null:
        walk.stop()
        
    if target:
        points = get_node("/root/World/Map/Map/Navigation2D").get_simple_path(get_global_position(), target, false)
    # if the path has more than one point
        if points.size() > 1:
            var distance = points[1] - get_global_position()
            direction = distance.normalized() # direction of movement
            if distance.length() > eps or points.size() > 2:
    #            set_linear_velocity(direction*speed)
                move_direction.x = int(direction.x)
                move_direction.y = int(direction.y)
                var animation = direction2str(direction)
                $Area2D.rotation = direction.angle()
                print(animation)
                walk.play("default-" + animation + "-8")   
                move = move_and_slide(direction * speed)
            else:
    #            set_linear_velocity(Vector2(0, 0)) # close enough - stop moving
                target = null
                move_and_slide(Vector2(0, 0))
        else:
            target = null
    update()

    
func direction2str(direction):
    var angle = direction.angle()
    if angle < 0:
        angle += 2 * PI
    var index = round(angle / PI * 4)
    if index == 8:
        index = 0
    return directions[index]
    
func _action():
    if contact != null && contact.is_in_group("action"):
        if occuped == false:
            contact.action()
            
func _push(velocity):
    speed = 200
    if contact != null && contact.is_in_group("push"):
        if occuped == false:
            speed = 80
            contact.push(velocity)
            
func fire(velocity):
    var path
    if selected_target != null:
        path = selected_target.global_position - $Area2D/Nuzzle.global_position
        if player.fire() == true:
            var a = Bullet.instance()
            a.start($Area2D/Nuzzle.global_position, path.normalized())
            get_parent().add_child(a)
        else:
            print("fail")
    else:
        
        if player.fire() == true:
            print("ok")
            var a = Bullet.instance()
            a.start($Area2D/Nuzzle.global_position, velocity.normalized())
            get_parent().add_child(a)
    
    

func _on_Area2D_body_entered(body):
#    print("Player touch -> " + body.get_name())
    contact = body
func _on_Area2D_body_exited(body):
    contact = null
func _on_Area2D_area_entered(area):
#    print("Player touch -> " + area.get_name())
    contact = area.get_parent()
func _on_Area2D_area_exited(area):
    contact = null

#le timer toute les seconde
func _on_Timer_timeout():
    if player && player.main == true:
        player.atb_increase(30)
        player.pm_increase(2)
    
func set_main():
    $Camera2D.current = true
    
func unset_main():
    $Camera2D.current = false

#quand un monstre approche
func _on_Area2D3_body_entered(body):
    if body.is_in_group("monster"):
        get_parent().add_monster_list(body)
        if selected_target == null:
            selected_target = _make_target(body)

#quand un monstre s'eloigne
func _on_Area2D3_body_exited(body):
    if body.is_in_group("monster"):
        var foe = get_parent().del_monster_list(body)
        if node_target != null:
            if foe != null:
                node_target.queue_free()
                node_target = null
                print(foe)
                selected_target = _make_target(foe)
            else:
                node_target.queue_free()
                node_target = null
                selected_target = null

# créer une ligne de ciblage du joueur vers l'ennemie ciblé
func _make_target(node):
    print("node_target")
    node_target = class_target.instance()
    node_target.width = 6.0
    node_target.set_start($Position2D)
    node_target.set_end(node.get_node("Position2D"))
    add_child(node_target)
    return node
