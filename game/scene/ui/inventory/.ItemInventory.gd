extends Control

const sqlite = preload("res://game/script/util/sqlite.gd");
var Item = preload("res://game/script/util/resource/item.gd")
var emplacement = preload("res://game/scene/ui/inventory/Emplacement.tscn")

var items = []

func _ready():
    items = sqlite.array("SELECT * FROM items")
    for i in items:
        add_item(i)
    
func add_item(item):
    var e = emplacement.instance()
    e.hint_tooltip = "nom : "+ item.name
    e.hint_tooltip += "\ndescription : "+ item.desc
    e.hint_tooltip += "\ntype : "+ str(item.type)
    e.hint_tooltip += "\nprix : "+ item.price + " or"
    var texture = ImageTexture.new()
    texture.Load(item.sprite);
    e.get_node("Sprite").texture = texture    
    $Panel/VBoxContainer/ScrollContainer/GridContainer3.add_child(e)
