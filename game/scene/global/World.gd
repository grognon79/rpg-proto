extends Node
#########################
var Player = preload("res://game/scene/game/player/Player.tscn")
var startMap = "Map01"
var current_map
var debug_mode = false
var players
var x = 60
var y = 60

func _ready():
    start_game()
    
func start_game():
    for i in Session.players:
        $Players.add_player(i)
    goto_map("res://game/scene/game/map/map_01.tscn")

func goto_scene(path):
    call_deferred("_deferred_goto_scene", path)

func goto_map(map_name):
    call_deferred("_deferred_goto_map", map_name)

func _deferred_goto_map(path):
    if current_map:
        current_map = $Map.get_child(1)
        current_map.free()
    var scene = ResourceLoader.load(path)
    current_map = scene.instance()
    current_map.set_name("Map")
    $Map.add_child(current_map)
    #get_tree().set_current_scene(current_map)
